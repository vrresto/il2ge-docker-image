LLVM_MINGW_ARCHIVE=llvm-mingw-${LLVM_MINGW_VERSION}-msvcrt-ubuntu-20.04-x86_64.tar.xz
LLVM_MINGW_BASE_URL=https://github.com/mstorsjo/llvm-mingw/releases/download
LLVM_MINGW_URL=${LLVM_MINGW_BASE_URL}/${LLVM_MINGW_VERSION}/${LLVM_MINGW_ARCHIVE}

MESA_BASE_URL=https://gitlab.freedesktop.org/mesa/mesa/-/archive/
MESA_URL=${MESA_BASE_URL}/mesa-${MESA_VERSION}/mesa-mesa-MESA_VERSION.tar.bz2

GLM_URL=https://github.com/g-truc/glm/archive/${GLM_VERSION}.tar.gz

HALF_BASE_URL=https://downloads.sourceforge.net/project/half/half
HALF_URL=${HALF_BASE_URL}/${HALF_VERSION}/half-${HALF_VERSION}.zip


set -ex


dpkg --add-architecture i386
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install $(xargs < packages.txt)
update-ca-certificates


mkdir -p /opt/llvm-mingw
curl -L $LLVM_MINGW_URL | tar --strip-components=1 -xJC /opt/llvm-mingw

mkdir -p /opt/src/mesa
curl -L  $MESA_URL| tar --strip-components=1 -xjC /opt/src/mesa

mkdir -p /opt/src/half
curl -L  $HALF_URL > half.zip
unzip half.zip -d /opt/src/half
rm half.zip

mkdir -p /opt/src/glm
curl -L  $GLM_URL | tar --strip-components=1 -xzC /opt/src/glm
