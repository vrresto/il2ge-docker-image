# syntax=docker/dockerfile:1

FROM ubuntu:22.04

ENV LLVM_MINGW_VERSION=20231128
ENV MESA_VERSION=18.1.6
ENV GLM_VERSION=0.9.9.0
ENV HALF_VERSION=2.1.0

ENV GLM_DIR=/opt/src/glm
ENV HALF_DIR=/opt/src/half
ENV MESA_DIR=/opt/src/mesa

COPY setup.bash /
COPY packages.txt /

RUN bash /setup.bash
